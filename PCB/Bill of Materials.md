# Bill of Materials
* ICs needed:
* LTC4054ES5-4.2
* MC34063AD
* LM324A
* 
* Resistors
* 1x1.65kΩ
* 1x330Ω
* 1x0.04Ω
* 1x180Ω
* 1x6.8kΩ
* 1x2.2kΩ
* 1x1.5kΩ
* 1x1kΩ
* 1x9.3kΩ
* 3x250Ω
* 1x3.25kΩ
* 4x150Ω
* 
* Capacitors
* 1x1microF
* 1x1.5nF
* 1x0.22mF
* 1x0.1mF
* 
* LED
* 5xNSCW100
* 
* Inductors
* 1x18.91microH Inductor	
* 
* Diodes
* 1xDiode
* 
* Misc
* Battery holder for Lithium-ion battery (18650 Li-ion cell	
* microPi HAT	
