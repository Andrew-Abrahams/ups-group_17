The LED design is functional and works to what is needed. It tells us the battery percentage level through the use of lit LEDs. These are the battery percentage levels that are indicated by the LEDs: 
All 4 LEDs are illuminated, the battery is fully charged.
* 3 LEDs are illuminated, the battery is between 75% charge and fully charged.
* 2 LEDs are illuminated, the battery is between 50% charge and 75% charge.
* 1 LED is illuminated, the battery is between 25% charge and 50% charge.
* 0 LEDs are illuminated, the battery is below 25% charge.
* 
* Therefore, this design is ideal in showing the user the battery percentage of the Lithium-Ion battery.
