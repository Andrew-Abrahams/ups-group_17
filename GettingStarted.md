# Setting up the microPi HAT
* To use this PI Hat all you have to do is to plug the hat into your 40 pin connector. Make sure it is the right way and that the 4 holes for the pins line up. Secure the board with the 4 provided pins that go in each corner.
* Connect the battery to the B+ and B- holes on the Hat. Make sure you connect the board correctly as you could damage the circuit. If it is all working fine the LEDs will come on, if not you may have connected a flat battery. Please use one 18650 Battery (not provided because of shipping).
* To load/read any code or information just plug in the PI on the main Board as the UPS port on the UPS Hat cant send any information between the Hat and the PI. 
* To charge the battery just plug the PI Hat into any USB port and see if the light is on.
